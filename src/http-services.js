import axios from 'axios'

const crudService = axios.create(
  {
    baseURL: '/api/api/crud/'
  }
)

export default {
}

export const CRUD_SERVICE = {
  async execute (method, resource, data) {
    // inject the accessToken for each request
    // let accessToken = await Vue.prototype.$auth.getAccessToken()
    return crudService({
      method,
      url: resource,
      data
    }).then(req => {
      return req.data
    })
  },
  getData (apiRoute) {
    return this.execute('get', apiRoute)
  },
  getDataById (apiRoute, id) {
    return this.execute('get', apiRoute + `/${id}`)
  },
  insertData (apiRoute, data) {
    return this.execute('POST', apiRoute, data)
  },
  updateData (apiRoute, id, data) {
    return this.execute('POST', apiRoute + `/${id}`, data)
  },
  deleteData (apiRoute, id) {
    return this.execute('delete', apiRoute + `/${id}`)
  }
}
