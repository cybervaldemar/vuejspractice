export let scheduleItems = [
  {
    id: 1,
    title: '1 schedule item',
    dateTime: new Date('2018-06-06'),
    description: 'first'
  },
  {
    id: 2,
    title: '2 schedule item',
    dateTime: new Date('2018-06-07'),
    description: 'first'
  },
  {
    id: 3,
    title: '3 schedule item',
    dateTime: new Date('2018-06-08'),
    description: 'first'
  },
  {
    id: 4,
    title: '4 schedule item',
    dateTime: new Date('2018-06-10'),
    description: 'first'
  }
]
