import Vue from 'vue'
import Router from 'vue-router'
import IndexPage from '@/IndexPage'
import ScheduleList from '@/components/ScheduleList'
import scheduleItem from '@/components/ScheduleItem'
import TablesList from '@/components/TablesList'
import TableItem from '@/components/TableItem'
import Login from '@/components/Login'
import AccessDenied from '@/components/AccessDenied'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'index-page',
      component: IndexPage
    },
    {
      path: '/login',
      name: 'login-page',
      component: Login
    },
    {
      path: '/tables',
      name: 'tables-list',
      component: TablesList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/table/:apiRoute',
      name: 'table-item',
      component: TableItem,
      meta: {
        requiresAuth: true,
        adminAccess: true
      }
    },
    {
      path: '/access-denied',
      name: 'access-denied',
      component: AccessDenied,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/schedule-list',
      name: 'schedule-list',
      component: ScheduleList
    },
    {
      path: '/schedule-list/:scheduleItemId',
      name: 'schedule-item',
      component: scheduleItem
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (sessionStorage.getItem('user') == null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      let userIsAdmin = JSON.parse(sessionStorage.getItem('isAdmin'))
      if (to.matched.some(record => record.meta.adminAccess)) {
        if (userIsAdmin === true) {
          next()
        } else {
          next({name: 'access-denied'})
        }
      } else {
        next()
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (sessionStorage.getItem('user') == null) {
      next()
    } else {
      next({name: 'tables-list'})
    }
  } else {
    next()
  }
})

export default router
